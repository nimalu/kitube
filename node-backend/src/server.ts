import express from 'express';
import cors from 'cors';
import {films, lectures} from './sampleData'
const PORT = 5000;

const app = express();
app.use(cors());

app.get('/films', (req, res) => {
    res.send(films)
});
app.get('/film/:id', (req, res) => {
    res.send(films.find(f => ""+f.id == req.params.id))
});
app.get('/lecture/:id', (req, res) => {
    res.send(lectures.find(l => ""+l.id == req.params.id))
});
app.listen(PORT, () =>
    console.log(`Example app listening on port ${PORT}!`),
);