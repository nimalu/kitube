enum Category {
    LECTURE = "Lecture",
    EXERCISE = "Exercise"
}
type FilmModel = {
    title: string;
    subtitle: string;
    streamUrl: string;
    thumbnailUrl: string;
    categories: Array<Category>;
    id: number;
    uploaded: number;
}
interface LectureModel {
    name: string;
    lastCrawl: number;
    shortName: string;
    authors: Array<String>;
    films: Array<FilmModel>;
    id: number;
}
export const films: Array<FilmModel> = [
    {
        id: 1,
        uploaded: Date.now() - 10000,
        title: "Eine Einführung in die Informatik",
        subtitle: "Das ist toll",
        streamUrl: "https://cdn.plyr.io/static/demo/View_From_A_Blue_Moon_Trailer-576p.mp4",
        thumbnailUrl: "https://ak.picdn.net/shutterstock/videos/28479340/thumb/7.jpg?ip=x480",
        categories: [Category.LECTURE]
    },
    {
        id: 2,
        title: "Kapitel 420: Gras",
        uploaded: Date.now(),
        subtitle: "Mach keinen Blödsinn",
        streamUrl: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4",
        thumbnailUrl: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/images/BigBuckBunny.jpg",
        categories: [Category.EXERCISE],
    }
]

export const lectures: Array<LectureModel> = [
    {
        id: 1,
        name: "Financial Data Science",
        shortName: "FDS",
        lastCrawl: Date.now() - 20000,
        authors: [
            "https://ps.ipd.kit.edu/img/ma_walter_tichy.png",
            "https://augusto.modanese.net/images/profile.png",
        ],
        films,
    }
]