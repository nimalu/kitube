package nimalu.util;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Niklas Manuel Lutze
 */
public class Config {
    private static final Logger logger = Logger.getLogger(Config.class.getName());
    private static final Properties properties = load();

    private Config() {
    }

    public static String get(String key) {
        return properties.getProperty(key);
    }

    public static String getOrDefault(String key, String def) {
        return properties.getOrDefault(key, def).toString();
    }

    private static Properties load() {
        final Properties properties = new Properties();
        try {
            Resources.processResource("config.properties", path -> {
                try {
                    properties.load(Files.newInputStream(path));
                } catch (IOException e) {
                    logger.log(Level.SEVERE, "Could not load config", e);
                }
            });
        } catch (URISyntaxException | IOException e) {
            logger.log(Level.SEVERE, "Could not load config", e);
        }

        return properties;
    }
}
