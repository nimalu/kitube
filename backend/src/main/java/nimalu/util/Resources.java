package nimalu.util;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.*;
import java.util.Collections;
import java.util.function.Consumer;

/**
 * @author Niklas Manuel Lutze
 */
public class Resources {
    private Resources() {
    }

    public static void processResource(URI uri, Consumer<Path> action) throws IOException {
        try {
            Path p = Paths.get(uri);
            action.accept(p);
        } catch (FileSystemNotFoundException ex) {
            try (FileSystem fs = FileSystems.newFileSystem(uri, Collections.emptyMap())) {
                Path p = fs.provider().getPath(uri);
                action.accept(p);
            }
        }
    }

    public static void processResource(String path, Consumer<Path> action) throws URISyntaxException, IOException {
        processResource(ClassLoader.getSystemResource(path).toURI(), action);
    }

    public static String sanitize(String name) {
        return name.replace(File.separatorChar, '-')
                .replace(" ", "")
                .replace(':', '-')
                .replace('.', '_')
                .replace('/', '-')
                .replace('+', '_');


    }

    public static Path getPath(String configKey) {
        return Paths.get(Config.get(configKey));
    }
}
