package nimalu.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author Niklas Manuel Lutze
 */
public class JsonUtils {
    private static final GsonBuilder gsonBuilder = new GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
            .setPrettyPrinting();
    private static Gson defaultGson;


    public static Gson getDefaultGson() {
        if (defaultGson == null) {
            defaultGson = gsonBuilder.create();
        }
        return defaultGson;
    }

    public static String toJson(Object obj) {
        return getDefaultGson().toJson(obj);
    }

}
