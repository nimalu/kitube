package nimalu.util;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author Niklas Manuel Lutze
 */
public class Web {

    public static String urlDecode(String encoded) {
        return URLDecoder.decode(encoded, StandardCharsets.UTF_8);
    }

    public static String urlEncode(String decoded) {
        return URLEncoder.encode(decoded, StandardCharsets.UTF_8);
    }

    public static Map<String, Object> decodeQueryParameters(URL url) {
        return Pattern.compile("&")
                .splitAsStream(url.getQuery())
                .map(s -> s.split("=", 2))
                .distinct()
                .map(o -> Map.entry(urlDecode(o[0]), urlDecode(o[1])))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1));
    }

    public static String encodeQueryParameters(Map<String, Object> parameters, boolean urlEncode) {
        if (urlEncode) {
            return parameters.keySet().stream()
                    .map(key -> urlEncode(key) + "=" + urlEncode(parameters.get(key).toString()))
                    .collect(Collectors.joining("&"));
        } else {
            return parameters.keySet().stream()
                    .map(key -> key + "=" + parameters.get(key).toString())
                    .collect(Collectors.joining("&"));
        }
    }

    public static String encodeQuery(String pageUrl, Map<String, Object> params) {
        return encodeQuery(pageUrl, params, true, true);
    }

    public static String encodeQuery(String pageUrl, Map<String, Object> params, boolean keepOld, boolean urlEncode) {
        try {
            URL url = new URL(pageUrl);
            if (keepOld) {
                Map<String, Object> oldQuery = decodeQueryParameters(url);
                params.forEach(oldQuery::put);
                return pageUrl.substring(0, pageUrl.length() - url.getQuery().length()) + encodeQueryParameters(oldQuery, urlEncode);
            } else {
                return pageUrl.substring(0, pageUrl.length() - url.getQuery().length()) + encodeQueryParameters(params, urlEncode);
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }
}
