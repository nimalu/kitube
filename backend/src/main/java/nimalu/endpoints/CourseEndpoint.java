package nimalu.endpoints;

import nimalu.socket.Endpoint;
import nimalu.socket.Session;
import nimalu.storage.StandardPersistenceService;

/**
 * @author Niklas Manuel Lutze
 */
public class CourseEndpoint {

    @Endpoint("courses")
    public void onMessage(Session session, String unused) {
        session.update(StandardPersistenceService.getInstance().loadCourses());
        session.close();
    }
}
