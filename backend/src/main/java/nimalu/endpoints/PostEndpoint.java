package nimalu.endpoints;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import nimalu.fetcher.CachedIliasFetcher;
import nimalu.fetcher.FetchException;
import nimalu.model.Course;
import nimalu.model.VideoPost;
import nimalu.page.IliasPage;
import nimalu.page.Page;
import nimalu.socket.Endpoint;
import nimalu.socket.Session;
import nimalu.storage.StandardPersistenceService;
import nimalu.storage.Store;
import nimalu.util.Resources;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.LinkedList;
import java.util.Optional;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * @author Niklas Manuel Lutze
 */
public class PostEndpoint {
    private final Logger logger = Logger.getLogger(getClass().getName());
    private final Store store = Store.getInstance();

    @Endpoint("posts/load")
    public void getPosts(Session session, String courseId) {
        store.loadPosts(courseId);
        session.close();
    }

    @Endpoint("posts/pullAll")
    public void fetchPosts(Session session, String courseId) {
        Queue<Page> pages = new LinkedList<>();
        Course course = store.findCourse(courseId);
        pages.offer(new IliasPage(course));
        for (int i = 0; i < 30 && !pages.isEmpty(); i++) {
            Page page = pages.poll();
            store.updatePage(courseId, page);
            try {
                page.scrape();
            } catch (FetchException e) {
                e.printStackTrace();
                continue;
            }
            page.getPosts().forEach(post -> {
                downloadThumbnail(courseId, (VideoPost) post);
                store.updatePost(courseId, post);
            });
            page.getSubPages().forEach(pages::offer);
        }
        session.close();
    }

    private void downloadThumbnail(String courseId, VideoPost post) {
        try (InputStream in = (new URL(post.getThumbnail())).openStream()) {
            Path path = Resources.getPath("dist.path").resolve(courseId);
            Files.createDirectories(path);
            path = path.resolve(Resources.sanitize(post.getTitle()) + "-thumbnail.jpg");
            Files.copy(in, path, StandardCopyOption.REPLACE_EXISTING);
            post.setThumbnail("http://localhost:8080/" + courseId + "/" + path.getFileName().toString());
        } catch (IOException e) {
            logger.log(Level.WARNING, "could not download thumbnail", e);
        }
    }


    @Endpoint("posts/listen")
    public void listen(Session session, String courseId) {
        store.registerPostListener(courseId, session::update);
    }


    @Endpoint("post/link")
    public void fetchStreamLink(Session session, String json) {
        JsonObject obj = new JsonParser().parse(json).getAsJsonObject();
        String openCastLink = obj.get("opencastLink").getAsString();
        String courseId = obj.get("courseId").getAsString();
        try {
            VideoPost post = StandardPersistenceService.getInstance().loadPosts(courseId).stream()
                    .filter(l -> l instanceof VideoPost)
                    .map(l -> (VideoPost) l)
                    .filter(l -> l.getOpencastLink().equals(openCastLink))
                    .findAny()
                    .orElseThrow(() -> new FetchException("Could not find post with url " + openCastLink));
            String streamUrl = fetchStreamUrl(post);
            post.setStreamUrl(streamUrl);
            store.updatePost(courseId, post);
            session.update(streamUrl);
        } catch (FetchException e) {
            e.printStackTrace();
        }
        session.close();
    }

    private String fetchStreamUrl(VideoPost post) throws FetchException {
        Element element = CachedIliasFetcher.getInstance().fetch(post.getOpencastLink());
        Elements scriptElements = element.getElementsByTag("script");
        String regex = "\\{\"streams\"[\\s\\S]+\"sources\":.*\"mp4\".*\"src\":\"([^}\"]*)\"";
        Pattern pattern = Pattern.compile(regex);
        Optional<Matcher> scriptElement = scriptElements.stream()
                .map(script -> pattern.matcher(script.html()))
                .filter(Matcher::find)
                .findFirst();
        if (scriptElement.isEmpty()) {
            throw new FetchException("Could not find paella config");
        }
        return scriptElement.get().group(1);
    }


}
