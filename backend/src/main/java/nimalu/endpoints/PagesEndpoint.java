package nimalu.endpoints;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import nimalu.fetcher.FetchException;
import nimalu.page.Page;
import nimalu.socket.Endpoint;
import nimalu.socket.Session;
import nimalu.storage.Store;

/**
 * @author Niklas Manuel Lutze
 */
public class PagesEndpoint {
    private final Store store = Store.getInstance();

    @Endpoint("pages/load")
    public void getPages(Session session, String courseId) {
        store.loadPages(courseId);
        session.close();
    }

    @Endpoint("pages/listen")
    public void listen(Session session, String courseId) {
        store.registerPageListener(courseId, page -> session.update(page.toDto()));
    }

    @Endpoint("pages/pull")
    public void pullPage(Session session, String json) {
        JsonObject obj = new JsonParser().parse(json).getAsJsonObject();
        String pageUrl = obj.get("url").getAsString();
        String courseId = obj.get("courseId").getAsString();

        Page page = store.findPage(courseId, pageUrl);
        try {
            page.scrape();
        } catch (FetchException e) {
            e.printStackTrace();
        }
        page.getPosts().forEach(p -> store.updatePost(courseId, p));
        page.getSubPages().forEach(p -> store.updatePage(courseId, p));
        session.close();
    }
}
