package nimalu.storage;

import nimalu.model.Course;
import nimalu.model.PageDto;
import nimalu.model.Post;
import nimalu.page.Page;

import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;

/**
 * @author Niklas Manuel Lutze
 */
public class Store {
    private static final PersistenceService persistenceService = StandardPersistenceService.getInstance();
    private static final Store instance = new Store();
    private final HashMap<String, Consumer<Page>> pageListener = new HashMap<>();
    private final HashMap<String, Consumer<Post>> postListener = new HashMap<>();


    private Store() {
    }

    public static Store getInstance() {
        return instance;
    }

    public void updatePage(Course course, Page page) {
        updatePage(course.getId(), page);
    }

    public void updatePage(String courseId, Page page) {
        persistenceService.save(courseId, page.toDto());
        if (pageListener.containsKey(courseId)) {
            pageListener.get(courseId).accept(page);
        }
    }

    public Course findCourse(String courseId) {
        return persistenceService.loadCourse(courseId);
    }

    public List<Course> getCourses() {
        return persistenceService.loadCourses();
    }

    public void updatePost(Course course, Post post) {
        updatePost(course.getId(), post);
    }

    public void updatePost(String courseId, Post post) {
        persistenceService.save(courseId, post);
        if (postListener.containsKey(courseId)) {
            postListener.get(courseId).accept(post);
        }
    }

    public void loadPosts(String courseId) {
        persistenceService.loadPosts(courseId).forEach(p -> updatePost(courseId, p));
    }

    public void loadPosts(Course course) {
        loadPosts(course.getId());
    }

    public void loadPages(String courseId) {
        persistenceService.loadPages(courseId).forEach(p -> updatePage(courseId, p.toPage()));
    }

    public void loadPages(Course course) {
        loadPages(course.getId());
    }

    public void registerPageListener(String courseId, Consumer<Page> listener) {
        pageListener.put(courseId, listener);
    }

    public void registerPageListener(Course course, Consumer<Page> listener) {
        registerPageListener(course.getId(), listener);
    }

    public void registerPostListener(String courseId, Consumer<Post> listener) {
        postListener.put(courseId, listener);
    }

    public void registerPostListener(Course course, Consumer<Post> listener) {
        registerPostListener(course.getId(), listener);
    }

    public Page findPage(String courseId, String pageUrl) {
        PageDto dto = persistenceService.loadPage(courseId, pageUrl);
        return dto.toPage();
    }
}
