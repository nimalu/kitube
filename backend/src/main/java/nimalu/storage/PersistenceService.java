package nimalu.storage;

import nimalu.model.Course;
import nimalu.model.PageDto;
import nimalu.model.Post;

import java.util.List;

/**
 * @author Niklas Manuel Lutze
 */
interface PersistenceService {
    void save(String courseId, PageDto page);

    void save(String courseId, Post post);

    List<Course> loadCourses();

    Course loadCourse(String courseId);

    List<Post> loadPosts(String courseId);

    List<PageDto> loadPages(String courseId);

    PageDto loadPage(String courseId, String parentPageUrl);

}
