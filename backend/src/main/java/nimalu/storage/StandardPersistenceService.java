package nimalu.storage;

import nimalu.model.Course;
import nimalu.model.PageDto;
import nimalu.model.Post;
import nimalu.model.VideoPost;
import nimalu.util.Config;
import nimalu.util.JsonUtils;
import nimalu.util.Resources;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.*;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * @author Niklas Manuel Lutze
 */
public class StandardPersistenceService implements PersistenceService {

    private static final List<Course> courses = List.of(
            new Course(
                    "Datenbanksysteme",
                    "DB",
                    "https://ilias.studium.kit.edu/goto.php?target=crs_1463481"
            ),
            new Course(
                    "Rechnernetze",
                    "RN",
                    "https://ilias.studium.kit.edu/goto.php?target=crs_1455593"
            ),
            new Course(
                    "Numerik",
                    "N",
                    "https://ilias.studium.kit.edu/ilias.php?ref_id=1466370&cmdClass=ilrepositorygui&cmdNode=uk&baseClass=ilRepositoryGUI"
            )
    );
    private static final PathMatcher jsonMatcher = FileSystems.getDefault().getPathMatcher("glob:**.json");
    private static final StandardPersistenceService instance = new StandardPersistenceService();
    private final Logger logger = Logger.getLogger(getClass().getName());
    private final Path storageDirectory = Paths.get(Config.getOrDefault("storage.path", "storage"));

    public static StandardPersistenceService getInstance() {
        return instance;
    }

    @Override
    public void save(String courseId, PageDto page) {
        try {
            Path typeDir = storageDirectory
                    .resolve(courseId)
                    .resolve("pages");
            Files.createDirectories(typeDir);
            Path file = typeDir.resolve(Resources.sanitize(Base64.getEncoder().encodeToString(page.getUrl().getBytes())) + ".json");
            BufferedWriter writer = Files.newBufferedWriter(file);
            writer.write(JsonUtils.toJson(page));
            writer.flush();
            writer.close();
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Could not save page", e);
        }
    }

    @Override
    public void save(String courseId, Post post) {
        try {
            Path typeDir = storageDirectory
                    .resolve(courseId)
                    .resolve(post.getClass().getSimpleName());
            Files.createDirectories(typeDir);
            Path file = typeDir.resolve(Resources.sanitize(post.getTitle()) + ".json");
            BufferedWriter writer = Files.newBufferedWriter(file);
            writer.write(JsonUtils.toJson(post));
            writer.flush();
            writer.close();
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Could not save post", e);
        }
    }


    @Override
    public List<Course> loadCourses() {
        return courses;
    }

    @Override
    public List<Post> loadPosts(String courseId) {
        Path typeDir = storageDirectory.resolve(courseId).resolve(VideoPost.class.getSimpleName());
        try {
            Files.createDirectories(typeDir);
            return Files.list(typeDir)
                    .filter(jsonMatcher::matches)
                    .map(this::readItem)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Could not load post", e);
            return Collections.emptyList();
        }
    }

    private VideoPost readItem(Path p) {
        try (Reader reader = Files.newBufferedReader(p)) {
            return JsonUtils.getDefaultGson().fromJson(reader, VideoPost.class);
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Could not read item " + p.getFileName().toString(), e);
            return null;
        }
    }

    @Override
    public Course loadCourse(String courseId) {
        return courses.stream().filter(c -> c.getAbbreviation().equals(courseId)).findAny().orElseThrow(() -> new RuntimeException("Not found"));
    }


    @Override
    public List<PageDto> loadPages(String courseId) {
        Path typeDir = storageDirectory
                .resolve(courseId)
                .resolve("pages");
        try {
            Files.createDirectories(typeDir);
            return Files.list(typeDir)
                    .filter(jsonMatcher::matches)
                    .map(this::readPage)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Could not load post", e);
            return Collections.emptyList();
        }
    }

    @Override
    public PageDto loadPage(String courseId, String pageUrl) {
        Path file = storageDirectory
                .resolve(courseId)
                .resolve("pages")
                .resolve(Resources.sanitize(Base64.getEncoder().encodeToString(pageUrl.getBytes())) + ".json");
        return readPage(file);
    }

    private PageDto readPage(Path p) {
        try (Reader reader = Files.newBufferedReader(p)) {
            return JsonUtils.getDefaultGson().fromJson(reader, PageDto.class);
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Could not read page " + p.getFileName().toString(), e);
            return null;
        }
    }


    /*private void processNewPost(ScrapedVideoPost post, List<VideoPost> loaded, Consumer<VideoPost> callback) {
        String thumbnailUrl = post.getThumbnail();
        try (InputStream in = (new URL(thumbnailUrl)).openStream()) {
            Path path = Resources.getPath("dist.path").resolve(course.getAbbreviation());
            Files.createDirectories(path);
            path = path.resolve(Resources.sanitize(post.getTitle()) + "-thumbnail.jpg");
            Files.copy(in, path, StandardCopyOption.REPLACE_EXISTING);
            thumbnailUrl = "http://localhost:8080/" + course.getAbbreviation() + "/" + path.getFileName().toString();
        } catch (IOException e) {
            logger.log(Level.WARNING, "could not download thumbnail", e);
        }
        VideoPost vid = new VideoPost(course.getId(), post.getTitle(), post.getOpencastLink(), thumbnailUrl);
        callback.accept(vid);
        loaded.add(vid);
    }*/
}
