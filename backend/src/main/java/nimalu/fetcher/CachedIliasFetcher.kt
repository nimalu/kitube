package nimalu.fetcher

import nimalu.util.Config
import nimalu.util.Resources
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import java.nio.file.Files
import java.util.*
import java.util.logging.Logger

/**
 * @author Niklas Manuel Lutze
 */
fun String.base64UrlEncode(): String {
    return Base64.getEncoder().encodeToString(toByteArray())
}

class CachedIliasFetcher private constructor() : IliasFetcher() {
    private val logger = Logger.getLogger(javaClass.name)
    private val cacheDirectory = Resources.getPath("cache")
    private val cacheLifetime = Config.getOrDefault("cache.lifetime", "300").toLong()

    @Throws(FetchException::class)
    override fun fetch(url: String): Document {
        try {
            val sanitized = Resources.sanitize(url.base64UrlEncode())
            val cachedPage = cacheDirectory.resolve(sanitized)
            if (Files.exists(cachedPage)) {
                val content = Files.readString(cachedPage)
                val cut = content.indexOf('#')
                val old = content.substring(0, cut).toLong()
                if (System.currentTimeMillis() / 1000 < old + cacheLifetime || true) {
                    logger.info("Loaded from cache $url")
                    return Jsoup.parse(content.substring(cut + 1, content.length))
                }
            }
            val document = super.fetch(url)
            Files.createDirectories(cacheDirectory)
            var content = "${System.currentTimeMillis() / 1000}#${document.html()}"
            Files.writeString(cachedPage, content)
            logger.info("Saved in cache $url")
            return document
        } catch (e: Exception) {
            logger.warning("Error while loading from cache $url")
        }
        return super.fetch(url)
    }

    companion object {
        @JvmStatic
        val instance = CachedIliasFetcher()
    }
}