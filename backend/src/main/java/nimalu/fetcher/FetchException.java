package nimalu.fetcher;

/**
 * @author Niklas Manuel Lutze
 */
public class FetchException extends Exception {
    public FetchException(String message) {
        super(message);
    }

    public FetchException(String message, Exception e) {
        super(message, e);
    }

    public FetchException(Exception e) {
        super(e);
    }
}
