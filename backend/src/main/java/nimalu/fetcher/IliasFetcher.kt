package nimalu.fetcher

import com.google.gson.JsonSyntaxException
import com.google.gson.reflect.TypeToken
import nimalu.util.Config
import nimalu.util.JsonUtils
import nimalu.util.Resources
import org.jsoup.Connection
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import java.nio.file.Files
import java.util.logging.Logger

/**
 * @author Niklas Manuel Lutze
 */
open class IliasFetcher internal constructor() : Fetcher {
    private val logger = Logger.getLogger(javaClass.name)
    private val cookieJar = mutableMapOf<String, String>()
    private val cookiePath = Resources.getPath("cookies")

    init {
        cookieJar.load()
    }

    override fun fetch(url: String): Document {
        return fetch(url, 0)
    }

    private fun Connection.Response.grabCookies(): Connection.Response {
        cookieJar.putAll(cookies())
        return this
    }

    @Throws(FetchException::class)
    private fun fetch(url: String, retryCount: Int): Document {
        logger.info("Fetching $url ($retryCount)")
        if (retryCount > 1) {
            throw FetchException("Could not authenticate after $retryCount tries")
        }
        return try {
            val doc = Jsoup.connect(url)
                .cookies(cookieJar)
                .execute()
                .grabCookies()
                .parse()
            if (doc.isUserAuthenticated()) {
                doc
            } else {
                authenticate()
                fetch(url, retryCount + 1)
            }
        } catch (e: Exception) {
            throw FetchException(e)
        }
    }

    private fun MutableMap<String, String>.save() {
        if (!Files.exists(cookiePath)) {
            Files.createFile(cookiePath)
        }
        Files.writeString(cookiePath, JsonUtils.getDefaultGson().toJson(this).toString())
        logger.info("Saved " + cookieJar.size + " cookies")
    }

    private fun MutableMap<String, String>.load() {
        cookieJar.clear()
        if (Files.exists(cookiePath)) {
            try {
                val read = JsonUtils.getDefaultGson()
                    .fromJson<MutableMap<String, String>>(
                        Files.readString(cookiePath),
                        TypeToken.getParameterized(
                            MutableMap::class.java,
                            String::class.java, String::class.java
                        ).type
                    )

                putAll(read)
            } catch (e: JsonSyntaxException) {
                logger.warning("Could not read cookie file: " + e.message)
            }
            logger.info("Loaded " + cookieJar.size + " cookies")
        }
    }

    private fun Document.isUserAuthenticated(): Boolean {
        return getElementById("userlog") != null
                || select("table[id*=tbl_xoct]").size > 0
                || getElementById("playerContainer") != null
    }

    private fun Document.isLoginSuccessful(): Boolean {
        return selectFirst("input[name=RelayState]") != null
                && selectFirst("input[name=SAMLResponse]") != null
    }

    private fun authenticate() {
        logger.info("Authenticating")
        cookieJar.load()
        var document = Jsoup.connect("https://ilias.studium.kit.edu/Shibboleth.sso/Login")
            .cookies(cookieJar)
            .method(Connection.Method.POST)
            .data("sendLogin", "1")
            .data("idp_selection", "https://idp.scc.kit.edu/idp/shibboleth")
            .data("target", "/shib_login.php")
            .data("home_organization_selection", "Mit KIT-Account anmelden")
            .execute()
            .grabCookies()
            .parse()
        if (!document.isLoginSuccessful()) {
            val form = document.selectFirst("form")
            val csrfToken = form.select("input[name=csrf_token]").`val`()
            val action = form.attr("action")
            document = Jsoup.connect("https://idp.scc.kit.edu$action")
                .cookies(cookieJar)
                .method(Connection.Method.POST)
                .data("csrf_token", csrfToken)
                .data("_eventId_proceed", "")
                .data("j_username", Config.get("ilias.username"))
                .data("j_password", Config.get("ilias.password"))
                .execute()
                .grabCookies()
                .parse()
        }
        Jsoup.connect("https://ilias.studium.kit.edu/Shibboleth.sso/SAML2/POST")
            .cookies(cookieJar)
            .method(Connection.Method.POST)
            .data("RelayState", document.selectFirst("input[name=RelayState]").`val`())
            .data("SAMLResponse", document.selectFirst("input[name=SAMLResponse]").`val`())
            .execute()
            .grabCookies()
        cookieJar.save()
        logger.info("Authentication finished")
    }

    companion object {
        val instance = IliasFetcher()
    }
}