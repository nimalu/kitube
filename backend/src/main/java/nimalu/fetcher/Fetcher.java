package nimalu.fetcher;

import org.jsoup.nodes.Document;

/**
 * @author Niklas Manuel Lutze
 */
public interface Fetcher {
    Document fetch(String url) throws FetchException;
}
