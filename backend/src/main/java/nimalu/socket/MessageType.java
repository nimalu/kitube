package nimalu.socket;

/**
 * @author Niklas Manuel Lutze
 */
public enum MessageType {
    START, UPDATE, END
}
