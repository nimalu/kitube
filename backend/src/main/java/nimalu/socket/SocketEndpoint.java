package nimalu.socket;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.util.Objects.requireNonNull;

/**
 * @author Niklas Manuel Lutze
 * @version 0.1
 */
@ServerEndpoint(value = "/",
        decoders = MessageInDecoder.class,
        encoders = JsonEncoder.class
)
public class SocketEndpoint {

    private static final TreeMap<String, AnnotatedEndpoint> endpoints = new TreeMap<>();
    private final Logger logger = Logger.getLogger(this.getClass().getName());
    private javax.websocket.Session session = null;


    public static void registerEndpoint(String route, AnnotatedEndpoint endpoint) {
        endpoints.put(route, endpoint);
    }


    private void sendMessage(MessageOut out) {
        requireNonNull(session);
        try {
            session.getBasicRemote().sendObject(out);
        } catch (IOException | EncodeException e) {
            logger.log(Level.SEVERE, "Could not send message " + out.toString(), e);
        }
    }

    @OnMessage
    public void onMessage(MessageIn messageIn) {
        logger.info("Received Message: " + messageIn.getRoute());
        if (!endpoints.containsKey(messageIn.getRoute())) {
            logger.warning("endpoint not found: " + messageIn.getRoute());
            return;
        }
        Session session = new Session(messageIn.getSessionId(), this::sendMessage);
        endpoints.get(messageIn.getRoute()).onMessage(session, messageIn.getBody());
    }


    @OnOpen
    public void onOpen(javax.websocket.Session session) {
        this.session = session;
        logger.info("Connected ... " + session.getId());
    }

    @OnClose
    public void onClose(javax.websocket.Session session, CloseReason closeReason) {
        this.session = null;
        logger.info(String.format("Session %s close because of %s", session.getId(), closeReason));
    }

    @OnError
    public void onError(Throwable t) {
        logger.log(Level.SEVERE, "An error occurred:", t);
    }
}
