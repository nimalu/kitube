package nimalu.socket

/**
 * @author Niklas Manuel Lutze
 * @version 0.1
 */
class MessageIn(var route: String, var sessionId: String, var body: String) {
    override fun toString(): String = "Path: $route\n$body"
}

class MessageOut(var session: String, var type: MessageType, var body: String) {
    override fun toString(): String = "$session-$type\n$body"
}