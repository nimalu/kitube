package nimalu.socket

import nimalu.util.JsonUtils
import java.util.function.Consumer

/**
 * @author Niklas Manuel Lutze
 */
class Session(private val id: String, private val send: Consumer<MessageOut>) {
    fun update(message: String) {
        send.accept(MessageOut(id, MessageType.UPDATE, message))
    }

    fun update(message: Any) {
        send.accept(MessageOut(id, MessageType.UPDATE, parse(message)))
    }

    private fun parse(message: Any): String = JsonUtils.getDefaultGson().toJson(message)


    fun close() {
        send.accept(MessageOut(id, MessageType.END, ""))
    }
}