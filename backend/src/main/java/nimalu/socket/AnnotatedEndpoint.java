package nimalu.socket;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author Niklas Manuel Lutze
 */
public class AnnotatedEndpoint {

    private final Method onMessage;
    private final Object instance;

    public AnnotatedEndpoint(Method onMessage, Object instance) {
        this.onMessage = onMessage;
        this.instance = instance;
    }

    public void onMessage(Session session, String body) {
        try {
            onMessage.invoke(instance, session, body);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
