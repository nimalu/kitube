package nimalu.socket;

import nimalu.util.JsonUtils;

import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

/**
 * @author Niklas Manuel Lutze
 * @version 0.1
 */
public class JsonEncoder implements Encoder.Text<MessageOut> {

    @Override
    public String encode(MessageOut object) {
        return JsonUtils.getDefaultGson().toJson(object);
    }

    @Override
    public void init(EndpointConfig config) {
    }

    @Override
    public void destroy() {
    }
}
