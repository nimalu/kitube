package nimalu.socket;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;

/**
 * @author Niklas Manuel Lutze
 * @version 0.1
 */
public class MessageInDecoder implements Decoder.Text<MessageIn> {

    private static final Gson gson = new Gson();

    @Override
    public MessageIn decode(String s) {
        JsonObject jsonObject = gson.fromJson(s, JsonObject.class);
        String path = jsonObject.get("route").getAsString();
        String body = jsonObject.get("body").getAsString();
        String session = jsonObject.get("sessionId").getAsString();
        return new MessageIn(path, session, body);
    }

    @Override
    public boolean willDecode(String s) {
        return s != null;
    }


    @Override
    public void init(EndpointConfig config) {

    }

    @Override
    public void destroy() {

    }
}
