package nimalu.model

import nimalu.page.IliasPage
import nimalu.page.OpenCastFolderPage
import nimalu.page.Page

/**
 * @author Niklas Manuel Lutze
 */
class PageDto(
    var url: String,
    var name: String,
    var type: PageType
) {
    fun toPage() = type.fromDto(this)
}

enum class PageType {
    ILIAS_PAGE {
        override fun fromDto(dto: PageDto): Page {
            return IliasPage(dto.name, dto.url)
        }
    },
    OPENCAST_FOLDER_PAGE {
        override fun fromDto(dto: PageDto): Page {
            return OpenCastFolderPage(dto.name, dto.url)
        }
    };

    abstract fun fromDto(dto: PageDto): Page
}
