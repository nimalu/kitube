package nimalu.model

import java.time.LocalDateTime

/**
 * @author Niklas Manuel Lutze
 */
class VideoPost(
    parentUrl: String,
    name: String,
    uploaded: LocalDateTime,
    var opencastLink: String,
    var thumbnail: String?,
    var streamUrl: String?,
) : Post(parentUrl, name, uploaded)

open class Post(
    var parentUrl: String,
    var title: String,
    var uploaded: LocalDateTime
)

