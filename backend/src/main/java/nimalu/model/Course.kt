package nimalu.model

/**
 * @author Niklas Manuel Lutze
 * @version 0.1
 */
class Course(
    var name: String,
    var abbreviation: String,
    var url: String,
) {
    fun getId(): String = abbreviation
}