package nimalu.page;

import nimalu.fetcher.FetchException;
import nimalu.model.PageType;
import nimalu.model.VideoPost;
import nimalu.util.Web;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author Niklas Manuel Lutze
 */
public class OpenCastFolderPage extends Page {


    public OpenCastFolderPage(String name, String url) {
        super(name, url);
    }

    private static List<String> scrapeTitles(Element element) {
        return element.select("th").stream()
                .map(el -> el.selectFirst("span"))
                .map(Element::html)
                .collect(Collectors.toList());
    }

    public void scrape() throws FetchException {
        Document document = getFetcher().fetch(getUrl());
        if (document.selectFirst("table[id^=tbl_xoct_]") == null) {
            document = scrapeFirstStage(document);
        }
        if (document.selectFirst("*[id^=tab_page_sel]") != null) {
            document = scrapeSecondStage(document);
        }
        scrapeThirdStage(document);
    }

    @Override
    protected PageType getType() {
        return PageType.OPENCAST_FOLDER_PAGE;
    }

    private Document scrapeSecondStage(Element element) throws FetchException {
        Pattern pattern = Pattern.compile("tbl_xoct_(.+)");
        String id = element.selectFirst("table[id^=tbl_xoct_]").id();
        if (!pattern.matcher(id).matches()) {
            logger.warning("Could not disable pagination");
            scrapeThirdStage(element);
        }
        String tableId = pattern.matcher(id).group(1);
        Map<String, Object> params = Map.of(
                String.format("tbl_xoct_%s_trows", tableId), 800,
                "cmd", "asyncGetTableGUI",
                "cmdMode", "asynch"
        );
        String fullUrl = Web.encodeQuery(getUrl(), params, true, false);
        return getFetcher().fetch(fullUrl);
    }

    private Document scrapeFirstStage(Element element) throws FetchException {
        Element aLink = element.selectFirst("#tab_series a");
        String fullLink = combineFullUrl(getUrl(), aLink);
        Map<String, Object> params = Map.of(
                "limit", 800,
                "cmd", "asyncGetTableGUI",
                "cmdMode", "asynch"
        );
        String fullUrl = Web.encodeQuery(fullLink, params, true, false);
        return getFetcher().fetch(fullUrl);
    }

    private void scrapeThirdStage(Element element) {
        Element table = element.selectFirst(".ilTableOuter");
        List<String> titles = scrapeTitles(table);
        int titleIndex = titles.indexOf("Titel");
        int previewIndex = titles.indexOf("Vorschau");
        int linkIndex = titles.indexOf("Aufzeichnungen");
        for (Element row : table.select("tbody tr")) {
            Elements tds = row.select("td");
            String title = tds.get(titleIndex).html();
            String thumbnail = tds.get(previewIndex).selectFirst("*[src]").attr("src");
            String link = combineFullUrl(getUrl(), tds.get(linkIndex).selectFirst("a[href]"));
            addPost(new VideoPost(getUrl(), title, LocalDateTime.now(), link, thumbnail, ""));
            //addSubPage(new VideoPlayerPagelink));
        }
    }


}
