package nimalu.page;

import nimalu.fetcher.CachedIliasFetcher;
import nimalu.fetcher.FetchException;
import nimalu.fetcher.Fetcher;
import nimalu.model.PageDto;
import nimalu.model.PageType;
import nimalu.model.Post;
import org.jsoup.nodes.Element;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;


/**
 * @author Niklas Manuel Lutze
 */
public abstract class Page {
    protected final Logger logger = Logger.getLogger(getClass().getName());
    private final String url;
    private final List<Post> posts = new ArrayList<>();
    private final List<Page> subPages = new ArrayList<>();
    private final String name;

    public Page(String name, String url) {
        this.name = name;
        this.url = url;
    }

    protected static String combineFullUrl(String url, Element aLink) {
        final String http = "http://";
        String withoutProtocol = url.substring(http.length() + 1);
        int i = withoutProtocol.indexOf('/');
        String path = i == -1 ? url : url.substring(0, i + http.length() + 1);
        return String.format("%s/%s", path, aLink.attr("href"));
    }

    public String getName() {
        return name;
    }

    public abstract void scrape() throws FetchException;

    protected void addSubPage(Page page) {
        subPages.add(page);
    }

    protected void addPost(Post post) {
        posts.add(post);
    }

    public List<Post> getPosts() {
        return posts;
    }

    protected Fetcher getFetcher() {
        return CachedIliasFetcher.getInstance();
    }

    public List<Page> getSubPages() {
        return subPages;
    }

    public String getUrl() {
        return url;
    }

    public PageDto toDto() {
        return new PageDto(url, name, getType());
    }

    protected abstract PageType getType();
}
