package nimalu.page;

/**
 * @author Niklas Manuel Lutze
 */
public class ScrapeException extends RuntimeException {
    public ScrapeException(String message) {
        super(message);
    }
}
