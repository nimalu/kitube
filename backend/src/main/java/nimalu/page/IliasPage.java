package nimalu.page;

import nimalu.fetcher.FetchException;
import nimalu.model.Course;
import nimalu.model.PageType;
import nimalu.util.Resources;
import nimalu.util.Web;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.logging.Level;

/**
 * @author Niklas Manuel Lutze
 */
public class IliasPage extends Page {

    public IliasPage(Course course) {
        super(course.getName(), course.getUrl());
    }

    public IliasPage(String name, String url) {
        super(name, url);
    }

    public void scrape() throws FetchException {
        Document document = getFetcher().fetch(getUrl());
        Elements links = document.select("a.il_ContainerItemTitle");
        for (Element aLink : links) {
            String fullUrl = Page.combineFullUrl(getUrl(), aLink);
            String name = Resources.sanitize(aLink.html());
            try {
                addSubPage(nextPage(aLink, fullUrl));
            } catch (ScrapeException e) {
                logger.log(Level.INFO, "Could not find scraper for " + name);
            }
        }
    }

    @Override
    protected PageType getType() {
        return PageType.ILIAS_PAGE;
    }

    private Page nextPage(Element aLink, String url) {
        URL u;
        try {
            u = new URL(url);
        } catch (MalformedURLException e) {
            throw new ScrapeException(url + " malformed");
        }
        Map<String, Object> params = Web.decodeQueryParameters(u);
        if (params.containsKey("target=file")) {
            throw new ScrapeException("File Scraper not implemented yet " + url);
            // File
        } else if (params.containsKey("ref_id")) {
            Element img = aLink.parents().stream()
                    .filter(el -> el.hasClass("ilContainerListItemOuter"))
                    .findAny()
                    .orElseThrow(() -> new ScrapeException("Could not find parent of " + url))
                    .selectFirst("img.ilListItemIcon");
            if (img == null) {
                throw new ScrapeException("Could not figure out type of " + url);
            }
            if (img.attr("alt").toLowerCase().contains("opencast")) {
                return new OpenCastFolderPage(aLink.html(), url);
            } else if (img.attr("alt").toLowerCase().contains("ordner")) {
                return new IliasPage(aLink.html(), url);
            }
            throw new ScrapeException(img.attr("alt") + " not implemented");
        }
        throw new ScrapeException("Could not figure out type of " + url);
    }


}
