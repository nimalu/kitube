package nimalu.main;


import nimalu.socket.AnnotatedEndpoint;
import nimalu.socket.Endpoint;
import nimalu.socket.SocketEndpoint;
import nimalu.util.Resources;
import org.glassfish.tyrus.server.Server;
import org.reflections.Reflections;
import org.reflections.scanners.MethodAnnotationsScanner;

import javax.websocket.DeploymentException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class App {
    public static void main(String[] args) {
        System.setProperty("java.util.logging.SimpleFormatter.format", "[%1$tT] [%4$s] [%3$-8s] %5$s %6$s%n");

        registerEndpoints();


        Server server = new Server("localhost", 8025, "/", null, SocketEndpoint.class);
        FileServer fileServer = new FileServer(Resources.getPath("dist.path"), 8080);
        try {
            server.start();
            fileServer.start();
            synchronized (server) {
                server.wait();
            }
        } catch (IOException | InterruptedException | DeploymentException e) {
            e.printStackTrace();
        } finally {
            server.stop();
            fileServer.stop();
        }
    }

    private static void registerEndpoints() {
        Set<Method> methods = new Reflections("nimalu", new MethodAnnotationsScanner()).getMethodsAnnotatedWith(Endpoint.class);
        Map<String, Object> instances = new HashMap<>();
        for (Method method : methods) {
            Class<?> declaringClass = method.getDeclaringClass();
            if (!instances.containsKey(declaringClass.getName())) {
                try {
                    instances.put(declaringClass.getName(), declaringClass.getDeclaredConstructor().newInstance());
                } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                    continue;
                }
            }
            Object instance = instances.get(declaringClass.getName());
            Endpoint endpoint = method.getAnnotation(Endpoint.class);
            SocketEndpoint.registerEndpoint(endpoint.value(), new AnnotatedEndpoint(method, instance));
        }
    }


}
