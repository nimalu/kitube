package nimalu.main

import fi.iki.elonen.NanoHTTPD
import nimalu.util.Web
import java.io.InputStream
import java.io.OutputStream
import java.net.HttpURLConnection
import java.net.URL
import java.nio.file.Files
import java.nio.file.Path
import java.util.logging.Level
import java.util.logging.Logger


/**
 * @author Niklas Manuel Lutze
 */
class FileServer(private val dist: Path, port: Int) : NanoHTTPD(port) {
    private val logger = Logger.getLogger(javaClass.name)

    override fun serve(session: IHTTPSession): Response {
        return if (session.uri.startsWith("/stream")) {
            stream(session)
        } else {
            serverStaticFile(session.uri.removePrefix("/"))
        }
    }


    private fun serverStaticFile(path: String): Response {
        val file = dist.resolve(Web.urlDecode(path))
        if (!Files.exists(file)) {
            val message = "$path not found"
            logger.log(Level.SEVERE, message)
            return newFixedLengthResponse(Response.Status.NOT_FOUND, MIME_PLAINTEXT, message)
        }
        try {
            val mime = when (path.substringAfterLast(".")) {
                "jpg" -> "image/jpeg"
                "jpeg" -> "image/jpeg"
                "png" -> "image/png"
                else -> "text/plain"
            }

            return newChunkedResponse(
                Response.Status.OK,
                mime,
                Files.newInputStream(file)
            )
        } catch (e: Exception) {
            val message = "Failed to load asset $path because $e"
            logger.log(Level.SEVERE, message, e)
            return newFixedLengthResponse(message)
        }
    }


    private fun HttpURLConnection.toResponse(): CustomResponse {
        val res = CustomResponse(Response.Status.lookup(responseCode), contentType, inputStream, contentLengthLong)
        res.headers["Access-Control-Allow-Origin"] = "*"
        fun copyHeaders(vararg names: String) {
            for (name in names)
                if (name in headerFields) res.headers[name] = headerFields[name]!![0]
        }
        copyHeaders("Content-Type", "Content-Range", "Accept-Ranges", "Content-Length", "Date")
        return res
    }

    class CustomResponse(
        status: IStatus?,
        mimeType: String?,
        data: InputStream?,
        totalBytes: Long
    ) : NanoHTTPD.Response(status, mimeType, data, totalBytes) {
        private val logger = Logger.getLogger(javaClass.name)

        val headers = mutableMapOf<String, String>()

        override fun send(outputStream: OutputStream) {
            val headerWriter = outputStream.bufferedWriter()
            headerWriter.append("HTTP/1.1 ").append(status.description).append("\r\n")
            for ((k, v) in headers) {
                headerWriter.write("$k: $v\r\n")
            }
            headerWriter.write("\r\n")
            headerWriter.flush()
            try {
                data.transferTo(outputStream)
            } catch (e: Exception) {
                logger.info("Could not transfer stream completely")
            }
            outputStream.flush()
            data.close()
        }
    }

    private fun stream(session: IHTTPSession): Response {
        val streamUrl = decodeParameters(session.queryParameterString)["target"]!![0].replace("\\", "")
        return stream(streamUrl, session)
    }

    private fun stream(streamUrl: String, session: IHTTPSession): Response {
        val connection: HttpURLConnection = URL(streamUrl).openConnection() as HttpURLConnection
        if ("range" in session.headers) connection.setRequestProperty("range", session.headers["range"])
        connection.connect()
        return connection.toResponse()
    }


}