plugins {
    application
    kotlin("jvm") version "1.5.0"
    id("com.github.johnrengelman.shadow") version "7.0.0"
}
tasks.withType<Jar> {
    manifest {
        attributes["Main-Class"] = "nimalu.main.App"
    }
}

repositories {
    // Use JCenter for resolving dependencies.
    mavenCentral()
}

dependencies {
    // Use JUnit Jupiter API for testing.
    //testImplementation("org.junit.jupiter:junit-jupiter-api:5.6.2")

    // Use JUnit Jupiter Engine for testing.
    //testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")

    // Websockets
    implementation("javax.websocket:javax.websocket-api:1.1")

    // JSON
    implementation("com.google.code.gson:gson:2.8.0")

    // Tyrus
    implementation("org.glassfish.tyrus:tyrus-container-grizzly:1.1")
    implementation("org.glassfish.tyrus:tyrus-server:1.17")
    implementation("org.glassfish.tyrus:tyrus-container-grizzly-server:1.17")

    //kotlin
    implementation(kotlin("stdlib-jdk8"))

    //reflections for annotation processing
    implementation("org.reflections:reflections:0.9.12")

    // json parser
    implementation("org.jsoup:jsoup:1.13.1")

    // webserver
    implementation("org.nanohttpd:nanohttpd:2.3.1")

}

application {
    mainClass.set("nimalu.main.App")
}

/*
tasks.test {
    // Use junit platform for unit tests.
    useJUnitPlatform()
}
*/

