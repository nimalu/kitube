import {createRouter, createWebHistory} from "vue-router";
import FilmView from "./views/FilmView.vue";
import LectureView from "./views/LectureView.vue";
import HomeView from "./views/HomeView.vue";


const routes = [
    {
        path: "/course/:courseId",
        name: "Course",
        component: LectureView
    },
    {
        path: "/course/:courseId/post/:postId",
        name: "Film",
        component: FilmView
    },
    {
        path: "/:catchAll(.*)",
        name: "NotFound",
        component: HomeView
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;