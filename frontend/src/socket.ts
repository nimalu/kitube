function createId(length: number) {
    let result = [];
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
        result.push(characters.charAt(Math.floor(Math.random() *
            charactersLength)));
    }
    return result.join('');
}

class Session<T> {
    readonly id: string
    readonly handler: Function
    results: Array<T>
    route: string
    finished = false

    constructor(id: string, route: string, handler: Function) {
        this.handler = handler
        this.id = id
        this.results = []
        this.route = route
    }

    handleResult(res: T) {
        this.results.push(this.handler(res))
    }
}

type MessageIn = {
    type: "UPDATE" | "END"
    session: string
    body: string
}

class Socket {
    private ws: WebSocket | undefined;
    private readonly sessions: Map<String, Session<any>>;
    private connected: boolean = false
    private nextId = 0

    constructor() {
        this.sessions = new Map<String, Session<any>>();
    }

    connect(url: string) {
        const ws = new WebSocket(url || "")
        ws.onerror = () => {
            console.log("Connection lost.")
            setTimeout(this.connect.bind(this), 1000, url)
        }
        ws.onmessage = this.onMessage.bind(this)
        ws.onopen = () => {
            this.connected = true
            console.log("Connection opened " + JSON.stringify(this))
        }
        this.ws = ws;
    }


    startSession(route: string, args: any, callback: Function): Promise<Array<any>> {
        return this.waitUntilOpen()
            .then(() => {
                const sessionId = "id-" + this.nextId++
                const session = new Session(sessionId, route, callback)
                this.sessions.set(session.id, session)
                this.ws?.send(JSON.stringify({route, sessionId, "body": args}))
                return session
            })
            .then(session => new Promise(resolve => {
                const wait = () => {
                    if (session.finished) resolve(session.results)
                    else setTimeout(wait, 100)
                }
                return wait()
            }))
    }

    private onMessage(event: MessageEvent) {
        try {
            const received = JSON.parse(event.data) as MessageIn
            if (received.type === "UPDATE") {
                this.sessions.get(received.session)!.handleResult(received.body)
            } else if (received.type === "END") {
                this.sessions.get(received.session)!.finished = true
            } else {
                console.log("Don't know message type " + received.type);
            }
        } catch (e) {
            console.log("Could not process received message " + event.data)
        }
    }

    private waitUntilOpen() {
        return new Promise<void>((resolve) => {
            const wait = () => {
                if (this.connected) return resolve()
                console.log("Waiting for socket to open " + JSON.stringify(this))
                setTimeout(wait, 1000)
            }
            wait()
        })
    }
}

const websocket = new Socket()
websocket.connect("ws://localhost:8025/")

export default websocket