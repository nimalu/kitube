import {ref, Ref} from "vue";
import websocket from "./socket";
import {Course} from "./model/Course";
import {VideoPost} from "./model/VideoPost";
import {Page} from "./model/Page";

class BackendClient {
    courses: Ref<Array<Course>> = ref([])
    currentCourseId: Ref<string> = ref("")
    posts: Ref<Array<VideoPost>> = ref([])
    pages: Ref<Array<Page>> = ref([])

    private listenPosts(courseId: string) {
        console.log("Listening to posts " + courseId)
        return websocket.startSession("posts/listen", courseId, (json: string) => {
            const post = JSON.parse(json) as VideoPost
            this.posts.value = this.posts.value.filter(p => p.opencastLink != post.opencastLink)
            this.posts.value.push(JSON.parse(json))
        })
    }

    private listenPages(courseId: string) {
        console.log("Listening to pages " + courseId)
        return websocket.startSession("pages/listen", courseId, (json: string) => this.pages.value.push(JSON.parse(json)))
    }

    loadCourses(): Promise<unknown> {
        console.log("Loading courses")
        this.courses.value.length = 0
        return websocket.startSession("courses", "", (c: string) => {
            this.courses.value = JSON.parse(c)
        })
    }

    setCurrentCourse(courseId: string) {
        this.currentCourseId.value = courseId;
        this.posts.value.length = 0
        this.pages.value.length = 0
        return Promise.all([this.listenPages(courseId), this.listenPosts(courseId), this.loadPosts(), this.loadPages()])
    }

    private loadPosts() {
        return websocket.startSession("posts/load", this.currentCourseId.value, () => {})
    }

    private loadPages() {
        return websocket.startSession("pages/load", this.currentCourseId.value, () => {})
    }

    pullAllPosts() {
        return websocket.startSession("posts/pullAll", this.currentCourseId.value, () => {})
    }

    fetchStreamLink(post: VideoPost): Promise<string> {
        if (post == null) return Promise.reject("post is null")
        return websocket.startSession("post/link", JSON.stringify(post), (link: any) => link).then(result => result[0])
    }


    pullPage(page: Page) {
        return websocket.startSession("pages/pull", JSON.stringify({courseId: this.currentCourseId.value, ...page}), () => {})
    }

}

const service = new BackendClient()
export default service
