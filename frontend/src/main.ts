import {createApp} from 'vue'
import App from './App.vue'
import './index.css'
import router from './router'

const clickOutside = {
    // @ts-ignore
    beforeMount: (el, binding) => {
        el.clickOutsideEvent = (event: Event) => {
            if (!(el == event.target || el.contains(event.target))) {
                binding.value();
            }
        };
        document.addEventListener("click", el.clickOutsideEvent);
    },
    // @ts-ignore
    unmounted: el => {
        document.removeEventListener("click", el.clickOutsideEvent);
    },
};

createApp(App).use(router)
    .directive('click-outside', clickOutside).mount('#app')
