export interface VideoPost {
    courseId: string;
    title: string;
    opencastLink: string;
    thumbnail: string;
    streamUrl: string;
}