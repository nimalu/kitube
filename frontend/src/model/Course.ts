export interface Course {
    name: string;
    abbreviation: string;
    link: string;
}
