export interface Page {
    url: string;
    name: string;
}